#include <iostream>
using namespace std;

class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        cout << '\n' << x << ' ' << y << ' ' << z << endl;
    }

    double showVectorSize()
    {
        return sqrt(x * x + y * y + z * z);
    }



};




int main()
{
    Vector Simple(5, 4, 2);
    Simple.Show();
    cout << Simple.showVectorSize() << endl;

    return 0;
}